
// State is not application state, only the state this reducer is responsible for
// state = null means if the arg is undefined, set it to null
export default function (state = null, action) {

  console.log("Action received in ruducer_active_books .. ");

  switch( action.type ) {
    case 'BOOK_SELECTED' :
      return action.payload;
  }

  return state;
}
