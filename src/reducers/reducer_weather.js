import { FETCH_WEATHER } from '../actions/index'

// State is not application state, only the state this reducer is responsible for
// state = xxx means if the arg is undefined, set it to xxx
// Why this one is called but no others? because in the index.js under /reducers, we register weather: WeatherReducer.
export default function (state = [], action) {

console.log("Action received in reducer_weather Why this one is called but no others? because in the index.js under /reducers, we register weather: WeatherReducer. ", action);

  switch( action.type ) {
    case FETCH_WEATHER :
      // never manipulate the state directly
      // such as state.push blah blah blah
      // instead, use state.concat
      // as this will return a new state  
      // below line is equal to return state.concat( [action.payload.data] );
      return [action.payload.data, ...state];
  }


  return state;
}
