import axios from 'axios';

const API_KEY = '1744324ef682e354c2e8cbf19f266e79';
const ROOT_URL = `https://api.openweathermap.org/data/2.5/forecast?&appid=${API_KEY}`;
export const FETCH_WEATHER = 'FETCH_WEATHER';


export function fetchWeather(city) {

  const url = `${ROOT_URL}&q=${city}`;
  const request = axios.get(url);

  console.log('request:',request);

  return {
    type: FETCH_WEATHER,
    payload: request
  }
}

/*
export function selectBook(book) {
  console.log('A book has been selected', book.title);
  // selectBook is an action creator, it need to return an actions
  // , an object with a type property
  return {
    type: 'BOOK_SELECTED',
    payload: book
  }
}
*/
