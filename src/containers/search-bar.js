// Standart import
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// Action Creator
import { fetchWeather } from '../actions/index';

// class component
class SearchBar extends Component {


  constructor(props){
    super(props);

    this.state = {term:''};

    // With below line, you don't need to write:
    // e => this.onSearchTermChange(e)
    // this is binding the context
    this.onSearchTermChange = this.onSearchTermChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  render () {
    return (
      <form onSubmit={this.onFormSubmit} className="input-group">
        <input
          placeholder="Enter your city"
          className="form-control"
          value={this.state.term}
          onChange={ this.onSearchTermChange }
        />
        <span className="input-group-btn">
          <button type="submit" className="btn btn-secondary">Submit</button>
        </span>
      </form>
    );
  }


  onSearchTermChange (event) {
    console.log(event.target.value);
    this.setState({term: event.target.value});
  }

  onFormSubmit (event) {
    event.preventDefault();

    this.props.fetchWeather(this.state.term);
    this.setState({term: ''});



  }

}

function mapStateToProps (state) {
  // whatever is returned will show up as mapStateToProps
  // inside of Booklist
  return {

  }
}


// Anything returned from this function will end up as props on the BookList container
function mapDispatchToProps(dispatch) {
  // Whenever selectBook is called, the result should be passed to all out reducers
  return bindActionCreators( {fetchWeather},dispatch );
}


// Promote BookList from a component to a container
// passing null in the first parameter means no need to manage the state
export default connect(null,mapDispatchToProps)(SearchBar);
