import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectBook } from '../actions/index';
import { bindActionCreators } from 'redux';

// class component
class BookDetail extends Component {



  render () {

    if (!this.props.book) {
      return <div>Pls select book.</div>
    }

    return (

      <div>
        <h3>Details for: </h3>
        <div>{this.props.book.title}</div>
        <a href={this.props.book.link}>{this.props.book.title}</a>
      </div>

    );
  }

}

function mapStateToProps (state) {
  // whatever is returned will show up as mapStateToProps
  // inside of Booklist
  return {
    book: state.activeBook
  }
}

/*
// Anything returned from this function will end up as props on the BookList container
function mapDispatchToProps(dispatch) {
  // Whenever selectBook is called, the result should be passed to all out reducers
  return bindActionCreators( {selectBook: selectBook},dispatch );
}
*/

// Promote BookList from a component to a container
export default connect(mapStateToProps)(BookDetail);
